/* Sección de calculadora */
const display = document.querySelector("#display");
const buttons = document.querySelectorAll(".buttons button");

buttons.forEach((item) => {
  item.onclick = () => {
    if (item.id == "clear") {
      display.innerText = "";
    } else if (item.id == "backspace") {
      let string = display.innerText.toString();
      display.innerText = string.substr(0, string.length - 1);
    } else if (display.innerText != "" && item.id == "equal") {
      display.innerText = eval(display.innerText);
    } else if (display.innerText == "" && item.id == "equal") {
      display.innerText = "";
      setTimeout(() => (display.innerText = ""), 2000);
    } else if (item.id == "%") {
      display.innerText = display.innerText / 100;
    } else if (item.id == ".") {
      if (!display.innerText.includes(".")) {
        display.innerText = display.innerText += ".";
      }
    } else if (item.id == "0") {
      if (display.innerText === "0") {
        return;
      }
      display.innerText += item.id;
    } else {
      display.innerText += item.id;
    }
  };
});

/* Sección de adivinanza */
var msg1 = document.getElementById('message1');
var msg2 = document.getElementById('message2');
var msg3 = document.getElementById('message3');

var num = Math.floor(Math.random() *10) +1;
var intentos = 0;
var numero_intentos = [];

function inicio() {
  var userInput = Math.round(document.getElementById('guess').value);

  if (userInput < 1 || userInput > 10) {
    alert('Ingresar solo números entre 1 y 10');
  } else if (isNaN(userInput)) {
    alert('Ingresar números enteros.')
  } else {
    intentos += 1;
    
    if(userInput < num) {
      msg1.textContent = 'Tu número es muy pequeño.';
      msg2.textContent = 'No de intento: ' + intentos;
      document.getElementById("guess").value = "";
    } else if (userInput > num) {
      msg1.textContent = 'Tu número es muy grande.';
      msg2.textContent = 'No de intento: ' + intentos;
      document.getElementById("guess").value = "";
    } else if (userInput == num) {
      msg1.textContent = '¡Haz adivinado!';
      msg2.textContent = 'El número fue: ' + num;
      msg3.textContent = 'Se realizarón: ' + intentos +' intentos';
      document.getElementById("guess").value = "";
    }
  }
}

function resetear() {
  num = Math.floor(Math.random() * 10) + 1;
  intentos = 0;
  numero_intentos = [];
  document.getElementById('guess').value = '';
  msg1.textContent = 'No. de intentos: 0';
  msg2.textContent = '';
  msg3.textContent = '';
}

/* Loteria */
let numeros = [];
let min = 1;
let max = 99;

function numeroUser() {
  let numero = Math.round(document.getElementById("numero").value);

  if (numeros.includes(numero)) {
    alert("El número ya ha sido ingresado, por favor ingrese otro número.");
    document.getElementById("numero").value = "";
  } else if (numeros.length === 6) {
    alert("Ya ha ingresado el número máximo.");
    document.getElementById("numero").value = "";
  } else if (numero > max) {
    alert("Numero no permitido");
  } else {
    numeros.push(numero);
    document.getElementById("numero").value = "";
  }
}

function jugar() {
  if (numeros.length < 6) {
    alert("Por favor, ingrese los 6 números para jugar.");
    return;
  }

  let loteria = [];

  while (loteria.length < 6) {
    let numero = Math.floor(Math.random() * max - min + 1) + min;

    if (loteria.indexOf(numero) === -1) {
      loteria.push(numero);
    }
  }

  let aciertos = 0;

  for (let i = 0; i < numeros.length; i++) {
    if (loteria.includes(numeros[i])) {
      aciertos++;
    }
  }

  document.getElementById("numeros-escogidos").textContent = numeros.join(", ");
  document.getElementById("numeros-aleatorios").textContent = loteria.join(", ");
  document.getElementById("aciertos").textContent = aciertos;
  if(aciertos === 6) {
    document.getElementById("aciertos").innerText = aciertos + " , Ganaste, sos rico!";
  } else {
    document.getElementById("aciertos").innerText = aciertos + " , Sigue intentándolo";
  }

  document.querySelector("form").style.display = "none";
  document.querySelector(".resultados").style.display = "";

  let volverAJugar = document.getElementById("volverJugar");
  volverAJugar.addEventListener("click", function() {
    document.querySelector("form").style.display = "";
    document.querySelector(".resultados").style.display = "none";
    numeros = [];
    document.getElementById("numeros-escogidos").textContent = "";
    document.getElementById("numeros-aleatorios").textContent = "";
    document.getElementById("aciertos").textContent = "";
  });
  document.querySelector(".resultados").appendChild(volverAJugar);
}

/* Lista de tareas */
// Definimos la lista de tareas
let tareas = [];

// Función para agregar tareas a la lista
function agregarTarea() {
  // Obtenemos el valor del input
  let nuevaTarea = document.getElementById("nuevaTarea").value;
  if (nuevaTarea === '') {
    alert('Por favor, ingrese una tarea');
  } else {
    // Agregamos la tarea a la lista
    tareas.push({
    tarea: nuevaTarea,
    completada: false
  });
  }
  // Limpiamos el input
  document.getElementById("nuevaTarea").value = "";
  // Actualizamos la lista de tareas en el HTML
  actualizarListaTareas();
}

// Función para marcar una tarea como completada
function marcarCompletada(index) {
  // Cambiamos el estado de completada de la tarea
  tareas[index].completada = !tareas[index].completada;
  // Actualizamos la lista de tareas en el HTML
  actualizarListaTareas();
}

// Función para eliminar una tarea de la lista
function eliminarTarea(index) {
  // Eliminamos la tarea de la lista
  tareas.splice(index, 1);
  // Actualizamos la lista de tareas en el HTML
  actualizarListaTareas();
}

// Función para actualizar la lista de tareas en el HTML
function actualizarListaTareas() {
  // Obtenemos el elemento <ul> donde se mostrarán las tareas
  let listaTareas = document.getElementById("listaTareas");
  // Borramos todas las tareas anteriores
  listaTareas.innerHTML = "";
  // Recorremos la lista de tareas y creamos un nuevo elemento <li> para cada una
  tareas.forEach(function(tarea, index) {
    let nuevoElemento = document.createElement("li");
    let checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.checked = tarea.completada;
    checkbox.addEventListener("change", function() {
      marcarCompletada(index);
    });
    let textoTarea = document.createTextNode(tarea.tarea);
    nuevoElemento.appendChild(checkbox);
    nuevoElemento.appendChild(textoTarea);
    // Creamos un botón para eliminar la tarea
    let botonEliminar = document.createElement("button");
    botonEliminar.innerHTML = "Eliminar";
    botonEliminar.addEventListener("click", function() {
      eliminarTarea(index);
    });
    nuevoElemento.appendChild(botonEliminar);
    listaTareas.appendChild(nuevoElemento);
  });
}

// Verificador
function calcularDV() {
  const rutSinDV = document.getElementById("rut").value.replace(/\./g, "").replace(/\-/g, ""); // Elimina puntos y guiones del RUT ingresado
  let suma = 0;
  let multiplicador = 2;

  // Suma los productos de los dígitos del RUT con los multiplicadores 2 a 7 (repetidos ciclicamente)
  for (let i = rutSinDV.length - 1; i >= 0; i--) {
    suma += multiplicador * parseInt(rutSinDV.charAt(i));
    multiplicador++;
    if (multiplicador === 8) {
      multiplicador = 2;
    }
  }

  // Calcula el dígito verificador como el complemento a 11 de la suma anterior
  const dv = 11 - (suma % 11);
  let dvString;

  if (dv === 11) {
    dvString = "0";
  } else if (dv === 10) {
    dvString = "K";
  } else {
    dvString = dv.toString();
  }

  // Muestra el resultado en pantalla
  document.getElementById("rut").value = "";
  alert(`El dígito verificador de tu RUT es: ${dvString}`);
}

